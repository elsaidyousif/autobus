import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Scroll } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SearchPlacesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-places',
  templateUrl: 'search-places.html',
})
export class SearchPlacesPage {
  @ViewChild('scrollWeb') scrollWeb: Scroll;
  fromAddress;
  fromLoc;
  toAddress;
  toLoc;
  buses = [];
  type;
  currentLoc;
  locAddress;
  universitID;
  page = 1
  hideMoreBtn = true
  noData = false
  routeItemToSearch;
  constructor(public helper: HelperProvider,public navCtrl: NavController, public navParams: NavParams, 
    public serviceApi: ServicesProvider, public translate:TranslateService, public storage: Storage) {
      // determine search source and destination for trip planning, unversity or saved route
    this.type = navParams.get("type")
    if(this.type == 1){
    this.fromLoc = navParams.get("formloc")
    this.fromAddress =  navParams.get("from")
    this.toAddress = navParams.get("to")
    this.toLoc = navParams.get("toloc")
    }
    else if(this.type == 2){
      this.fromAddress = navParams.get("locAddress")
      this.fromLoc = navParams.get("currentLoc")
      this.toAddress = navParams.get("university_name")
      this.currentLoc = navParams.get("currentLoc")
      this.locAddress = navParams.get("locAddress")
      this.toLoc = navParams.get("univID")
      this.universitID = navParams.get("univID")
    }
    else{
      this.routeItemToSearch = navParams.get("routeItem")
    this.fromAddress = this.routeItemToSearch.from_location_address
    this.toAddress = this.routeItemToSearch.to_location_address
    }
  }

  // Function executed when view loaded
  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPlacesPage');
    if(!navigator.onLine){
      this.helper.presentToast(this.translate.instant("internetError"))
      return
    }
    // this.type == 1 means user determine source and destination else search unversity routes
    if(this.type == 1){
    this.storage.get("user_token").then(val => {
      let access = val ? val.access_token : null
      //alert(access)
      this.serviceApi.searchResults(1,this.page,access,this.helper.currentLang,this.fromAddress,this.fromLoc,
        this.toAddress,this.toLoc,this.helper.transType,(data)=>{
          if(!data.success){
            if(data.status == -6){
              this.noData = true
              this.hideMoreBtn = true
              this.helper.presentToast(this.translate.instant("outOfZone"))
              return;
            }
            else if(data.status == -5){
              this.noData = true
              this.hideMoreBtn = true
              this.helper.presentToast(this.translate.instant("sameSourceAnndDestination"))
              return;
            }
            else if(data.status == -7){
                if(data.errors.workTime)
                this.helper.presentAlert(data.errors.workTime)
                return;
              }
          }
          console.log(JSON.stringify(data))
          if(!data.data.data){
            this.noData = true
            this.hideMoreBtn = true
            return;
          }
          if(this.page == 1 && data.data.data.length == 0){
            this.noData = true
            this.hideMoreBtn = true
          }
          //this.buses = data.data.data;
          if(data.data.data.length > 0){
            data.data.data.forEach(element => {
            this.buses.push(element)
          });
          this.page += 1;
          if(data.data.data.length < data.data.per_page){
            this.hideMoreBtn = true
          }
          else{
            this.hideMoreBtn = false
          }
          }
          else{
            this.hideMoreBtn = true
          }
          
        },(data)=>{
          if(data.name == "TimeoutError"){
            this.helper.presentToast(this.translate.instant("TimeoutError"))
          }
          else{
            this.helper.presentToast(this.translate.instant("serverErr"))
          }
        }) 
    })
  }
  else if(this.type == 2){
    this.storage.get("user_token").then(val => {
      let access = val ? val.access_token : null
      this.serviceApi.searchUniversityResults(2,this.toAddress,this.page,this.helper.transType,access,this.helper.currentLang,this.fromLoc,this.universitID,
        this.locAddress,(data)=>{
          if(!data.success){
            if(data.status == -6){
              this.noData = true
              this.hideMoreBtn = true
              this.helper.presentToast(this.translate.instant("outOfZone"))
              return;
            }
            else if(data.status == -5){
              this.noData = true
              this.hideMoreBtn = true
              this.helper.presentToast(this.translate.instant("sameSourceAnndDestination"))
              return;
            }
            else if(data.status == -7){
                if(data.errors.workTime)
                this.helper.presentAlert(data.errors.workTime)
                return;
            }
          }
          console.log(JSON.stringify(data))
          if(!data.data.data){
            this.noData = true
            this.hideMoreBtn = true
            return;
          }
          if(this.page == 1 && data.data.data.length == 0){
            this.noData = true
          }
          if(data.data.data.length > 0){

          data.data.data.forEach(element => {
            
            this.buses.push(element)
          });
          this.page += 1;
          if(data.data.data.length < data.data.per_page){
            this.hideMoreBtn = true
          }
          else{
            this.hideMoreBtn = false
          }
        }
        },(data)=>{
          if(data.name == "TimeoutError"){
            this.helper.presentToast(this.translate.instant("TimeoutError"))
          }
          else{
            this.helper.presentToast(this.translate.instant("serverErr"))
          }
        }) 
    })
  }

  }
  // Function to open trip details view
  openTripDetails(item){
    this.navCtrl.push("TripDetailsPage",{details:item, type: this.type , fromAddress: this.fromAddress , fromLoc : this.fromLoc, toLoc: this.toLoc, toAddress: this.toAddress})
  }

}
